import React from 'react';
import PropTypes from 'prop-types';
import Grid from '../../common/components/Grid/Grid';
import GifItem from '../../common/components/GifItem/GifItem';
import './Favorited.css';

class Favorited extends React.PureComponent {
  static displayName = 'Favorited';

  static propTypes = {
    localStoredFavoritedIds: PropTypes.arrayOf(PropTypes.string).isRequired,
    fetchedFavoritedGifs: PropTypes.arrayOf(PropTypes.object).isRequired,
    toggleGifFromFavorited: PropTypes.func.isRequired,
    refreshFavoritedGifs: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.refreshFavoritedGifs();
  }

  render() {
    return (
      <main role="main" className="favorited">
        <Grid>
          {this.props.fetchedFavoritedGifs.map((gif, index) =>
            <GifItem gifData={gif} key={`${gif.id}-${index}`}>
              <GifItem.Gif url={gif.images.preview_gif.url} />
              <GifItem.ActionsLayer
                isFavorited={this.props.localStoredFavoritedIds.includes(
                  gif.id
                )}
                onClickHandler={() => {
                  this.props.toggleGifFromFavorited(gif);
                }}
              />
            </GifItem>
          )}
        </Grid>
      </main>
    );
  }
}

export default Favorited;
