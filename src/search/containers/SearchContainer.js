/* global URLSearchParams */

import { compose } from 'redux';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { searchRequest } from '../ducks';
import Search from '../components/Search';

const mapStateToProps = ({ routing }) => ({
  initialValues: {
    searchTerms: new URLSearchParams(routing.location.search).get('q')
  }
});

const mapDispatchToProps = dispatch => ({
  searchRequestHandler: ({ searchTerms }) =>
    dispatch(searchRequest(searchTerms, true))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'SEARCH_FORM',
    enableReinitialize: true
  })
)(Search);
