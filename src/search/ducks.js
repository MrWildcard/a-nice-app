/* eslint-disable no-undef */

import { createModule } from 'moducks';
import { put, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import client from '../common/client';
import * as sagas from './sagas';

const initialState = {
  fetchedGifs: [],
  loading: false
};

const {
  search,
  sagas: ducksSagas,
  searchRequest,
  searchRequestSuccess
} = createModule(
  'search',
  {
    SEARCH_REQUEST: {
      creator: [
        searchTerms => ({ searchTerms }),
        (payload, pushLocation = false) => ({ pushLocation })
      ],
      reducer: state => ({
        ...state,
        loading: true
      }),
      *saga(action) {
        const { payload, meta } = action;

        if (!payload.searchTerms) {
          return;
        }

        const encodedTerms = encodeURIComponent(payload.searchTerms);

        const { data: { data } } = yield call(client.get, 'gifs/search', {
          params: {
            q: encodedTerms
          }
        });

        if (meta.pushLocation) {
          yield put(
            push({
              pathname: '/',
              search: `?q=${encodedTerms}`
            })
          );
        }

        yield put(searchRequestSuccess(data));
      }
    },

    SEARCH_REQUEST_SUCCESS: {
      creator: data => ({ gifs: data }),
      reducer: (state, action) => ({
        ...state,
        loading: false,
        fetchedGifs: action.payload.gifs
      })
    }
  },
  initialState
);

const searchSagas = {
  ...ducksSagas,
  ...sagas
};

export { searchSagas, searchRequest };

export default search;
