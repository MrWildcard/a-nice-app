import React, { PureComponent } from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';

class Search extends PureComponent {
  static displayName = 'Search';

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    searchRequestHandler: PropTypes.func.isRequired,
    pristine: PropTypes.bool.isRequired
  };

  render() {
    const { pristine, handleSubmit, searchRequestHandler } = this.props;

    return (
      <div className="Search">
        <form onSubmit={handleSubmit(searchRequestHandler)}>
          <label htmlFor="searchTerms">Rechercher des GIFs</label>
          <Field
            type="search"
            name="searchTerms"
            id="searchTerms"
            component="input"
          />
          <input
            type="submit"
            onClick={handleSubmit(searchRequestHandler)}
            disabled={pristine}
          />
        </form>
      </div>
    );
  }
}

export default Search;
