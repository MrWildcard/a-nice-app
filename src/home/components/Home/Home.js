import React from 'react';
import PropTypes from 'prop-types';
import Grid from '../../../common/components/Grid/Grid';
import GifItem from '../../../common/components/GifItem/GifItem';
import './Home.css';

class Home extends React.PureComponent {
  render() {
    return (
      <main className="home" role="main">
        <Grid>
          {this.props.fetchedGifs.map((gif, index) =>
            <GifItem gifData={gif} key={`${gif.id}-${index}`}>
              <GifItem.Gif url={gif.images.fixed_width.url} />
              <GifItem.ActionsLayer
                isFavorited={this.props.localStoredFavoritedIds.includes(
                  gif.id
                )}
                onClickHandler={() => {
                  this.props.toggleGifFromFavorited(gif);
                }}
              />
            </GifItem>
          )}
        </Grid>
      </main>
    );
  }
}

Home.displayName = 'Home';

Home.propTypes = {
  fetchedGifs: PropTypes.arrayOf(PropTypes.object).isRequired,
  localStoredFavoritedIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  toggleGifFromFavorited: PropTypes.func.isRequired
};

export default Home;
