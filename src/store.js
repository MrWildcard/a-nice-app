/* globals window */
/* eslint-disable no-underscore-dangle */

/**
 * From https://github.com/moducks/moducks/blob/master/examples/npmSearch/src/store.js
 */
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { all } from 'redux-saga/effects';
import { flattenSagas } from 'moducks/es';
import createHistory from 'history/createBrowserHistory';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import app, { appSagas } from './app/ducks';
import search, { searchSagas } from './search/ducks';
import favorited, { favoritedSagas } from './favorited/ducks';

export const history = createHistory();

export default function configureStore() {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    combineReducers({
      app,
      search,
      favorited,
      routing: routerReducer,
      form: formReducer
    }),
    composeEnhancers(applyMiddleware(routerMiddleware(history), sagaMiddleware))
  );

  return {
    ...store,
    runSaga: () =>
      sagaMiddleware.run(function* rootSaga() {
        yield all(
          flattenSagas({
            appSagas,
            searchSagas,
            favoritedSagas
          })
        );
      })
  };
}
