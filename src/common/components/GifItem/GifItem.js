import React from 'react';
import PropTypes from 'prop-types';
import cs from 'classnames';
import './GifItem.css';

const GifItem = props => {
  const rootCSSClassnames = cs('gif-item', props.customCSSClassname);
  const { gifData: { images: { fixed_width } } } = props;

  return (
    <div
      style={{
        width: `${fixed_width.width}px`,
        height: `${fixed_width.height}px`
      }}
      className={rootCSSClassnames}
    >
      {props.children}
    </div>
  );
};

GifItem.propTypes = {
  gifData: PropTypes.shape({
    images: PropTypes.shape({
      fixed_width: PropTypes.shape({
        width: PropTypes.string.isRequired,
        height: PropTypes.string.isRequired
      })
    })
  }).isRequired,
  customCSSClassname: PropTypes.string,
  children: PropTypes.node
};

GifItem.Gif = props =>
  <div className="gif">
    <img src={props.url} alt="Giphy-Resultat-Recherche" />
  </div>;

GifItem.Gif.displayName = 'GifItem.Gif';

GifItem.Gif.propTypes = {
  url: PropTypes.string.isRequired
};

GifItem.ActionsLayer = props => {
  const rootElementCSSClassnames = cs('gif-actions', {
    favorited: props.isFavorited
  });

  return (
    <div className={rootElementCSSClassnames}>
      <button className="gif-action" onClick={props.onClickHandler} />
    </div>
  );
};

GifItem.ActionsLayer.displayName = 'GifItem.ActionsLayer';

GifItem.ActionsLayer.propTypes = {
  isFavorited: PropTypes.bool.isRequired,
  onClickHandler: PropTypes.func.isRequired
};

GifItem.ActionsLayer.defaultProps = {
  isFavorited: false
};

export default GifItem;
