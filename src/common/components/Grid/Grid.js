import React from 'react';
import PropTypes from 'prop-types';
import Bricks from 'bricks.js';

class Grid extends React.PureComponent {
  componentDidMount() {
    this.gridInstance = Bricks({
      container: this.rootDOMElement,
      packed: 'data-packed',
      sizes: [{ columns: 4, gutter: 20 }]
    });

    if (this.props.children.length) {
      this.gridInstance.pack();
    }
  }

  componentDidUpdate() {
    if (this.props.children.length) {
      this.gridInstance.pack();
    }
  }

  gridInstance = null;
  rootDOMElement = null;

  render() {
    return (
      <section
        className="grid"
        ref={element => (this.rootDOMElement = element)} // eslint-disable-line no-return-assign
      >
        {this.props.children}
      </section>
    );
  }
}

Grid.propTypes = {
  children: PropTypes.node.isRequired
};

export default Grid;
