/* global afterEach, describe, it, expect, localStorage */

import { getArrayFromLocalStorage } from '../utils';

describe('utils', () => {
  afterEach(() => {
    localStorage.clear();
  });

  describe('getArrayFromLocalStorage', () => {
    it('should return a valid array without values separator', () => {
      localStorage.setItem('test', ['a', 'b']);

      expect(getArrayFromLocalStorage('test')).toEqual(['a', 'b']);
    });

    it('should return a valid array with a custom value separator', () => {
      localStorage.setItem('test', ['a', 'b'].join('|'));

      expect(getArrayFromLocalStorage('test', '|')).toEqual(['a', 'b']);
    });

    it('should return an empty array from unexisting local storage key', () => {
      expect(getArrayFromLocalStorage('__')).toEqual([]);
    });

    it('...even with a custom splitter', () => {
      expect(getArrayFromLocalStorage('__', 'T')).toEqual([]);
    });
  });
});
