# A nice WebApp
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

## Status

|                                                                       master                                                                       |                                                                        release                                                                       |                                                                        develop                                                                       |
|:--------------------------------------------------------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------:|
| [![pipeline status](https://gitlab.com/MrWildcard/a-nice-app/badges/master/pipeline.svg)](https://gitlab.com/MrWildcard/a-nice-app/commits/master) | [![pipeline status](https://gitlab.com/MrWildcard/a-nice-app/badges/release/pipeline.svg)](https://gitlab.com/MrWildcard/a-nice-app/commits/release) | [![pipeline status](https://gitlab.com/MrWildcard/a-nice-app/badges/develop/pipeline.svg)](https://gitlab.com/MrWildcard/a-nice-app/commits/develop) |
| [![coverage report](https://gitlab.com/MrWildcard/a-nice-app/badges/master/coverage.svg)](https://gitlab.com/MrWildcard/a-nice-app/commits/master) | [![coverage report](https://gitlab.com/MrWildcard/a-nice-app/badges/release/coverage.svg)](https://gitlab.com/MrWildcard/a-nice-app/commits/release) | [![coverage report](https://gitlab.com/MrWildcard/a-nice-app/badges/develop/coverage.svg)](https://gitlab.com/MrWildcard/a-nice-app/commits/develop) |

## You won't believe it

🤥

## Troubleshooting

- Debug what's going on in CI locally :

```
$ docker run -ti --rm -v `pwd`:/code -w /code DOCKER_IMAGE
```

Replace `DOCKER_IMAGE` by the docker image name used in CI job.  